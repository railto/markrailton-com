const colors = {
  primary: '#F74E00', // Color for buttons or links
  bg: '#fff', // Background color
  white: '#fff',
  grey: {
    dark: '#2E3D52',
    default: '#2E3D52',
    light: 'rgba(0, 0, 0, 0.5)',
    ultraLight: 'rgba(0, 0, 0, 0.25)',
  },
};

const transitions = {
  normal: '0.5s',
};

const fontSize = {
  small: '0.9rem',
  big: '2.9rem',
};

export default {
  colors,
  transitions,
  fontSize,
};
